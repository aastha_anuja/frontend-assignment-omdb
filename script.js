$(document).ready(() => {
  $("#movieForm").on("submit", (e) => {
    let movieSearch = $("#movieSearch").val();
    getMovies(movieSearch);
    e.preventDefault();
  });
});


function getMovies(movieSearch) {
  axios
    .get("https://www.omdbapi.com?s=" + movieSearch + "&apikey=11b96d6e")
    .then((response) => {
      console.log(response);
      let movies = response.data.Search;
      let output = "";
      $.each(movies, (index, movie) => {
        output += `
            <div class="col-md-3">
            <div class="well text-center">
            <div class="card mt-4 box-shadow-hover pointer" style="width: 16rem;">
            <img class="card-img-top" src="${movie.Poster}" alt="Card image cap">
            <div class="card-body">
            <h6 class="card-title" style="text-align: left;">${movie.Title}</h6>
            <a onclick="movieSelected('${movie.imdbID}')" class="btn btn-primary" style="margin-right: 11rem"; href="movie.html">Details</a>
            </div>
            </div>
            </div>
            </div>
            `;
      });

      $("#result").html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

function movieSelected(id) {
  sessionStorage.setItem("movieId", id);
  window.location = "movie.html";
  return false;
}

function getResult() {
  let movieId = sessionStorage.getItem("movieId");

  axios
    .get("https://www.omdbapi.com?i=" + movieId + "&apikey=11b96d6e")
    .then((response) => {
      console.log(response);
      let movie = response.data;

      let output = `
        <div class="row">
        <div class="col-md-4">
        <div class="card">
        <img class="card-img-right" src="${movie.Poster}" class="thumbnail img-responsive">
        </div>
        </div>
        <div class="col-md-8">
        <div class ="card">
        <div class="card-body">
        <h5 class="card-title">${movie.Title}</h5>
        <ul class="list-group">
        <li class="list-group-item"><strong>Genre:</strong> ${movie.Genre}</li>
        <li class="list-group-item"><strong>Released:</strong> ${movie.Released}</li>
        <li class="list-group-item"><strong>Ratings:</strong> ${movie.Rated}</li>
        <li class="list-group-item"><strong>IMDb Rating:</strong> ${movie.imdbRating}</li>
        <li class="list-group-item"><strong>Director:</strong> ${movie.Director}</li>
        <li class="list-group-item"><strong>Writer:</strong> ${movie.Writer}</li>
        <li class="list-group-item"><strong>Actors:</strong> ${movie.Actors}</li>
        <li class="list-group-item"><strong>Plot:</strong> ${movie.Plot}</li>
        </ul>
        
        <a href="https://imdb.com/title/${movie.imdbID}" target="_blank" class="btn btn-primary">View IMDB</a>
        <a href="index.html" class="btn btn-primary">Go Back To Search</a>
        </div>
        </div>
        </div>
        </div>
        `;
      $("#movie").html(output);
    })
    .catch((err) => {
      console.log(err);
    });
}

